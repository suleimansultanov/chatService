import { Form, Button, FormControl, InputGroup } from 'react-bootstrap';
import { useState } from 'react';

const SendPrivateMessageForm = ({ sendMessage }) => {
    const [privateMessage, setPrivateMessage] = useState('');
    const [user, setUser] = useState('');

    return <>
    <Form
    onSubmit={e => {
        e.preventDefault();
        sendMessage(privateMessage, user);
        setPrivateMessage('');
    }}>
    <InputGroup>
        <FormControl type="user" placeholder="private message..."
            onChange={e => setPrivateMessage(e.target.value)} value={privateMessage} />
                    <FormControl type="user" placeholder="to..."
            onChange={e => setUser(e.target.value)} value={user} />
        <InputGroup.Append>
        
            <Button variant="primary" type="submit" disabled={!user || !privateMessage}>Send</Button>
        </InputGroup.Append>
    </InputGroup>
    </Form>
</>
}

export default SendPrivateMessageForm;