import SendMessageForm from './SendMessageForm';
import MessageContainer from './MessageContainer';
import ConnectedUsers from './ConnectedUsers';
import { Button } from 'react-bootstrap';
import SendPrivateMessageForm from './SendPrivateMessage';

const Chat = ({ sendMessage, messages, users, closeConnection, sendPrivateMessage }) => <div>
    <div className='leave-room'>
        <Button variant='danger' onClick={() => closeConnection()}>Leave Room</Button>
    </div>
    <ConnectedUsers users={users} />
    <div className='chat'>
        <MessageContainer messages={messages} />
        <SendMessageForm sendMessage={sendMessage} />
        <SendPrivateMessageForm sendMessage={sendPrivateMessage} />
    </div>
</div>

export default Chat;