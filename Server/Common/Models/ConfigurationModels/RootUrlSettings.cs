﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.ConfigurationModels
{
    public class RootUrlSettings
    {
        public string RootUrl { get; set; }
    }
}
