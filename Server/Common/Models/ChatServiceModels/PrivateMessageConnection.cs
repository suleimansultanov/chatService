﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.ChatServiceModels
{
    public class PrivateMessageConnection : UserConnection
    {
        public string TargetConnectionId { get; set; }
        public string Message { get; set; }
    }
}
